# Hellom210 CD - Beispiel Kubernetes

Damit wir die Webanwendung hellom210 in Kubernetes betreiben können benötigt es im Minimum drei Dienste. Ein Deployment, ein Service und ein Ingress-Controller.

Das Deployment erstellt die Pods. Ähnlich wie bei Docker die Container. Häufig wird dazu ein Docker-Image verwendet.

Der Service macht die Pods im Cluster verfügbar damit andere Dienste auf die Pods zugreifen können. Es ist quasi wie ein Mini-Gateway der die Verbindungen zu den unterschiedlichen Pods managed.

Der Ingress-Controller sorgt dafür, dass ein Service extern im Netzwerk verfügbar ist. Es öffnet also ein Port auf der K8s-Cluster IP auf dem wir dann auf den Dienst zugreiffen können.

## Den Dienst in K8s manuell starten

- Docker Umgebungsvariable so einstellen, dass die Docker-Befehle im Minikube und nicht auf dem Host ausgeführt werden.  
  `eval $(minikube docker-env)`
- Docker Image herunterladen  
  `docker login registry.gitlab.com -u username -p accesstoken`  
  `docker pull registry.gitlab.com/username/projekt/hellom210:latest`
- K8s Namespace erstellen und diesen verwenden  
  `kubectl create namespace hellom210`  
  `kubectl config set-context --current --namespace=hellom210`
- K8s deployment erstellen  
  `kubectl create deployment hellom210-web --image=registry.gitlab.com/username/projekt/hellom210:latest`
- K8s Service bereitstellen  
  `kubectl expose deployment hellom210-web --port=80 --target-port=80 --name=hellom210-service`
- Damit wir auf hellom210 Zugriff haben müssen wir einen Port-Forward machen  
  `kubectl port-forward service/hellom210-service 8080:80`
- Teste ob du auf die Webapp zugreifen kannst.  
  `curl http://127.0.0.1:8080`
- Aufräumen  
   `kubectl delete service hellom210-web-service`  
   `kubectl delete deployment hellom210-web`  
  oder einfach alles gleichzeitig löschen:  
  `kubectl delete namespace hellom210`

## Den Dienst in K8s mit Ingress-Controller starten mit YML-Files

- Falls noch kein Ingress-Controller in Minikube aktiv ist, diesen aktivieren  
  `minikube addons enable ingres`

- Den Namespace für die Applikation erstellen  
  `kubectl create namespace hellom210`

- Pods erstellen:  
  `kubectl apply -f hellom210-deployment.yml`

- Service erstellen:  
  `kubectl apply -f hellom210-service.yml`

- Ingress-Controller erstellen:  
  `kubectl apply -f hellom210-ingress.yml`

- K8s Cluster IP anzeigen:  
  `minikube ip`

- Teste ob du auf die Webapp zugreifen kannst.  
  `curl http://<minikubeip>:8080`

## Den Dienst in K8s starten mit ArgoCD

- Unser Gitlab Repository hinzufügen. Zuerst Access Token erstellen.  
  `argocd repo add https://gitlab.com/dein-repository.git --username YOUR_GITLAB_USERNAME --password YOUR_PERSONAL_ACCESS_TOKEN`

- Den Namespace für die Applikation erstellen  
  `kubectl create namespace hellom210`

- Die Applikation in ArgosCD erstellen  
  `argocd app create hellom210 --repo https://gitlab.com/dein-repository.git --path . --dest-server https://kubernetes.default.svc --dest-namespace hellom210`

- Die Applikation mit einem Sync starten  
  `argocd app sync hellom210`

- Den Sync automatisieren (alle 3 Minuten sync ist default)  
  `argocd app set hellom210 --sync-policy automated`
